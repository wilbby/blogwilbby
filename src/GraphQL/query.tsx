import gql from "graphql-tag";

const GET_POSTS = gql`
  query getPosts($country: String, $page: Int) {
    getPosts(country: $country, page: $page) {
      message
      success
      data {
        id
        title
        image
        shortDescription
        like
        country
        tags
        author
        Author {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          OnesignalID
        }
        category
        readTime
        slug
        content
        created_at
      }
    }
  }
`;

const GET_POSTS_FOR_TAG = gql`
  query getPostsForTags($country: String, $page: Int, $tag: String) {
    getPostsForTags(country: $country, page: $page, tag: $tag) {
      message
      success
      data {
        id
        title
        image
        shortDescription
        like
        country
        tags
        author
        Author {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          OnesignalID
        }
        category
        readTime
        slug
        content
        created_at
      }
    }
  }
`;

const GET_POST = gql`
  query getPostbyId($slug: String, $country: String) {
    getPostbyId(slug: $slug, country: $country) {
      message
      success
      data {
        id
        title
        image
        shortDescription
        like
        country
        tags
        author
        Author {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          OnesignalID
        }
        category
        readTime
        slug
        content
        created_at
      }
    }
  }
`;

const GET_POST_SINGLE = gql`
  query getPost($country: String) {
    getPost(country: $country) {
      message
      success
      data {
        id
        title
        image
        shortDescription
        like
        tags
        country
        author
        Author {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          OnesignalID
        }
        category
        readTime
        slug
        content
        created_at
      }
    }
  }
`;

export const query = {
  GET_POSTS,
  GET_POST,
  GET_POST_SINGLE,
  GET_POSTS_FOR_TAG,
};
