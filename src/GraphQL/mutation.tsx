import gql from "graphql-tag";

const CREATE_POST = gql`
  mutation createPost($input: PostInput) {
    createPost(input: $input) {
      success
      message
    }
  }
`;

const ACTUALIZAR_POST = gql`
  mutation actualizarPost($input: PostInput) {
    actualizarPost(input: $input) {
      success
      message
      data {
        id
      }
    }
  }
`;

const ELIMINAR_POST = gql`
  mutation eliminarPost($id: ID!) {
    eliminarPost(id: $id) {
      success
      message
    }
  }
`;

export const mutation = { CREATE_POST, ACTUALIZAR_POST, ELIMINAR_POST };
