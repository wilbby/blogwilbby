import React from "react";
import animationData from "../../assets/animate/nodata.json";
import Lottie from "react-lottie";
import "./index.css";

export default function NoData() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <div className="nodata">
      <Lottie options={defaultOptions} height={300} width={300} />
      <h3>No hay nada para ti :(</h3>
    </div>
  );
}
