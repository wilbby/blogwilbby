import React from "react";
import Logo from "../../assets/img/logoblog.svg";
import { Link } from "react-router-dom";
import Curve from "../../assets/img/curve.svg";

import "./index.css";

var TxtType = function (el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function () {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function () {
    that.tick();
  }, delta);
};

window.onload = function () {
  var elements = document.getElementsByClassName("typewrite");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-type");
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtType(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".typewrite > .wrap { border-right: 0.10em solid #2d2861}";
  css.innerHTML = ".typewrite { color: #fff}";
  document.body.appendChild(css);
};

export default function Header(props) {
  const { city, locality } = props;
  return (
    <div className="header_cont">
      <div className="header">
        <div className="logo">
          <Link to="/">
            <img src={Logo} alt="Wilbby Blog" />
          </Link>
        </div>
        <div className="Links">
          <a href="https://wilbby.com" className="buttons" target="_blak">
            Empezar
          </a>
        </div>
      </div>
      <div className="info_header">
        <div>
          <h2>Blog {city ? city : ""}</h2>
          <p>
            Somos el equipo de prensa, contenido, investigación y diseño de
            Wilbby. Ayudamos a digitalizar las tiendas de barrio para que de
            está manera puedan llegar a más clientes con nuestra potente app.
          </p>
          <a href={`https://wilbby.com`}>
            Descubre establecimientos cerca de ti
          </a>
        </div>
      </div>
      <img src={Curve} style={{ marginBottom: -1 }} className="curve" />
    </div>
  );
}
