import React from "react";
import "./index.css";

export default function Franja() {
  return (
    <div className="franja">
      <div className="franja_cont">
        <h1>Lee nuestro blog</h1>
        <p>
          Cosas en las que estamos trabajando, eventos a los que vamos,
          productos que usamos y personas que conocemos.
        </p>
      </div>
    </div>
  );
}
