import React from "react";
import "./index.css";
import AppLogo from "../../assets/img/icon.png";
import { Rate } from "antd";

export default function GetApp() {
  return (
    <div className="getapp_container">
      <div className="getapp_container_app">
        <div className="_app">
          <img src={AppLogo} alt="Wilbby" />
          <div className="_name">
            <h3>Wilbby</h3>
            <Rate
              disabled
              defaultValue={4.5}
              style={{ fontSize: 14 }}
              allowHalf
            />
          </div>
        </div>
        <div className="_app__btn">
          <a href="http://onelink.to/mf5tg4">Descargar app</a>
        </div>
      </div>
    </div>
  );
}
