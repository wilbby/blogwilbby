import React from "react";
import { useQuery } from "react-apollo";
import { query } from "../../GraphQL";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  ClockCircleOutlined,
  TagOutlined,
  CalendarOutlined,
  LikeOutlined,
} from "@ant-design/icons";
import { Tag, Tooltip } from "antd";
import "./index.css";
import moment from "moment";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import Loading from "../Loading/LoadingMain";
require("moment/locale/es");

function MainPost(props: any) {
  const { city } = props;

  const { data, loading, refetch } = useQuery(query.GET_POST_SINGLE, {
    variables: { country: city },
  });

  console.log(data);

  const post = data && data.getPost ? data.getPost.data : {};

  const author = post ? post.Author : {};

  if (loading) {
    return <Loading />;
  }
  return (
    <div className="post_cont">
      <div className="post_cont_column">
        <div className="imagencon">
          <Tag
            color="lime"
            style={{ zIndex: 100, position: "absolute", margin: 20 }}
          >
            Post destacado
          </Tag>
          <img src={post.image} alt={post.title} />
        </div>
        <div className="info_post">
          <h1>{post.title}</h1>
          <p>{post.shortDescription}...</p>
          <span>
            <ClockCircleOutlined /> Lectura de {post.readTime}
          </span>

          <div style={{ marginTop: 10 }}>
            <span>
              <TagOutlined />
              {post.tags &&
                post.tags.map((t: string, i: number) => {
                  return (
                    <Tooltip title={`Ver posts sobre ${t}`} key={i}>
                      <Link to={`/tag/${t}`} className="tag">
                        {t},
                      </Link>
                    </Tooltip>
                  );
                })}
            </span>
          </div>

          <div style={{ marginTop: 10 }}>
            <span>
              <LikeOutlined /> Me gusta {post.like}
            </span>
          </div>

          <div style={{ marginTop: 10 }}>
            <span>
              <CalendarOutlined /> Publicado{" "}
              {moment(post.created_at).format("LL")}
            </span>
          </div>

          <div className="authorMain">
            <img
              src={IMAGES_PATH + author.avatar}
              alt={author.name}
              className="avatarMain"
            />
            <div className="namesMain">
              <h4>
                {author.name} {author.lastName}
              </h4>
              <p>Autor</p>
            </div>
          </div>

          <div className="links">
            <div>
              <FacebookShareButton url={"https://blog.wilbby.com/" + post.slug}>
                <FacebookIcon
                  size={40}
                  round={true}
                  style={{ marginLeft: 10, outline: "none" }}
                />
              </FacebookShareButton>

              <TwitterShareButton url={"https://blog.wilbby.com/" + post.slug}>
                <TwitterIcon
                  size={40}
                  round={true}
                  style={{ marginLeft: 10, outline: "none" }}
                />
              </TwitterShareButton>

              <LinkedinShareButton url={"https://blog.wilbby.com/" + post.slug}>
                <LinkedinIcon
                  size={40}
                  round={true}
                  style={{ marginLeft: 10, outline: "none" }}
                />
              </LinkedinShareButton>

              <WhatsappShareButton url={"https://blog.wilbby.com/" + post.slug}>
                <WhatsappIcon
                  size={40}
                  round={true}
                  style={{ marginLeft: 10, outline: "none" }}
                />
              </WhatsappShareButton>
            </div>
          </div>
          <div className="btn_content">
            <a href={post.content} target="_black">
              Leer más
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(MainPost);
