import React, { useState } from "react";
import "./index.css";
import {
  TwitterOutlined,
  InstagramFilled,
  FacebookFilled,
  LinkedinFilled,
  TransactionOutlined,
  MailOutlined,
  RightOutlined,
  PhoneOutlined,
  EnvironmentOutlined,
  ShopOutlined,
  ShoppingOutlined,
} from "@ant-design/icons";
import { Select, message, Modal } from "antd";
import SimpleMap from "../Maps";

import MainLogo from "../../assets/img/icon.png";
import Android from "../../assets/img/google.png";
import Apple from "../../assets/img/apple.svg";

const { Option } = Select;

const Footer = () => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <div className="Footer">
        <div className="Footer-Content">
          <div className="SuBitems">
            <img
              src={MainLogo}
              style={{ width: 60, marginBottom: 15, borderRadius: 15 }}
            />
            <div>
              <p className="text">Tu ciudad digital.</p>
            </div>
            <div style={{ display: "flex", width: "100%" }}>
              <a
                target="_blank"
                href="https://play.google.com/store/apps/details?id=com.foodyapp"
              >
                <img
                  src={Android}
                  className="logos googl"
                  style={{ marginRight: 15 }}
                />
              </a>
              <a
                target="_blank"
                href="https://apps.apple.com/es/app/wilbby-comida-a-domicilio/id1553798083"
              >
                <img src={Apple} className="logos googl" />
              </a>
            </div>
            <div style={{ display: "flex", marginTop: 20 }}>
              <a
                href="https://twitter.com/wilbby_ES"
                className="icons"
                target="_blank"
              >
                <TwitterOutlined style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.facebook.com/wilbbyapp.es"
                className="icons"
                target="_blank"
              >
                <FacebookFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.instagram.com/wilbby_es"
                className="icons"
                target="_blank"
              >
                <InstagramFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
              <a
                href="https://www.linkedin.com/company/wilbbyapp/"
                className="icons"
                target="_blank"
              >
                <LinkedinFilled style={{ fontSize: 22, marginRight: 15 }} />
              </a>
            </div>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">Acerca de Wilbby®</span>
              </li>
              <li>
                <a href="https://stores.wilbby.com">Une tu restaurante</a>
              </li>
              <li>
                <a href="https://wilbby.com/riders">Reparte con Wilbby</a>
              </li>
              <li>
                <a href="https://wilbby.com/#ventajas">Ventajas</a>
              </li>
              <li>
                <a href="https://wilbby.com/team">Nuestro equipo</a>
              </li>
              <li>
                <a href="https://blog.wilbby.com/">Blog</a>
              </li>
              <li>
                <a
                  onClick={() =>
                    message.warning("Aún no tenemos vacantes disponible")
                  }
                >
                  Trabaja con nosotros
                </a>
              </li>
            </ul>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">Obtener ayuda</span>
              </li>
              <li>
                <a
                  onClick={() => setVisible(true)}
                  style={{ cursor: "pointer" }}
                >
                  Contacto
                </a>
              </li>
              <li>
                <a href="https://wilbby.com/preguntas-frecuentes">
                  Preguntas frecuentes
                </a>
              </li>
              <li>
                <a href="https://wilbby.com/politica-de-privacidad">
                  Privacidad
                </a>
              </li>
              <li>
                <a href="https://wilbby.com/condiciones-de-uso">
                  Condiciones de uso
                </a>
              </li>
              <li>
                <a href="https://wilbby.com/cookies">Cookies</a>
              </li>
            </ul>
          </div>
          <div className="SuBitems">
            <Select defaultValue="españa" style={{ width: 140 }}>
              <Option value="españa"> 🇪🇸 España</Option>
            </Select>
            <Select
              defaultValue="español"
              style={{ width: 140, marginTop: 20, color: "#212121" }}
              bordered={false}
            >
              <Option value="idioma">
                {" "}
                <TransactionOutlined
                  style={{ color: "#90C33C", marginRight: 5 }}
                />{" "}
                Idioma
              </Option>
              <Option value="español"> 🇪🇸 Español</Option>
            </Select>
          </div>
        </div>
        <div className="Subfooter" />
        <div className="certificados">
          <p style={{ color: "gray", marginRight: 20 }}>
            {" "}
            © 2021 Hecho con 😍 en Tomelloso Wilbby App®
          </p>
        </div>
      </div>
      <Modal
        title="Contacto"
        visible={visible}
        footer={false}
        onCancel={() => setVisible(false)}
      >
        <SimpleMap
          lat={39.1582846}
          lgn={-3.0215836}
          width="100%"
          height={200}
          title="Wilbby®"
        />
        <div style={{ margin: 20, paddingBottom: 30 }}>
          <div className="item_contact">
            <MailOutlined
              style={{ marginRight: 15, color: "#90C33C", fontSize: 22 }}
            />{" "}
            <a
              href="mailto://info@Wilbby.com"
              style={{ color: "black" }}
              target="_blank"
            >
              info@Wilbby.com
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <PhoneOutlined
              style={{ marginRight: 15, color: "#90C33C", fontSize: 22 }}
            />{" "}
            <a href="tel:+34664028161" style={{ color: "black" }}>
              Hablar con el equipo de Wilbby®
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <EnvironmentOutlined
              style={{ marginRight: 15, color: "#90C33C", fontSize: 22 }}
            />{" "}
            <a href="/" style={{ color: "black" }}>
              Tomelloso ES
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <ShopOutlined
              style={{ marginRight: 15, color: "#90C33C", fontSize: 22 }}
            />{" "}
            <a href="https://stores.wilbby.com" style={{ color: "black" }}>
              Une tu restaurante
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <ShoppingOutlined
              style={{ marginRight: 15, color: "#90C33C", fontSize: 22 }}
            />{" "}
            <a href="https://wilbby.com/riders" style={{ color: "black" }}>
              Reparte con Wilbby
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
        </div>
      </Modal>
    </>
  );
};

export default Footer;
