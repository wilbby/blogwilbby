import React from "react";
import { useQuery, useMutation } from "react-apollo";
import { query, mutation } from "../../GraphQL";
import { Link } from "react-router-dom";
import {
  ClockCircleOutlined,
  TagOutlined,
  CalendarOutlined,
  LikeOutlined,
} from "@ant-design/icons";
import { Tooltip, message } from "antd";
import "./index.css";
import moment from "moment";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";
import "./index.css";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import Loading from "../Loading/Loading";
import Nodata from "../NoData";
require("moment/locale/es");
export default function Post(props: any) {
  const [actualizarPost] = useMutation(mutation.ACTUALIZAR_POST);
  const { city, page } = props;
  const { data, loading, refetch } = useQuery(query.GET_POSTS, {
    variables: { country: city, page: page },
  });

  const posts = data && data.getPosts ? data.getPosts.data : [];

  if (loading) {
    return (
      <div className="post_container_grid">
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
        <Loading />
      </div>
    );
  }

  const updatePost = (datos: any) => {
    const input = {
      id: datos.id,
      title: datos.title,
      image: datos.image,
      shortDescription: datos.shortDescription,
      like: datos.like + 1,
      tags: datos.tags,
      author: datos.author,
      category: datos.category,
      readTime: datos.readTime,
      content: datos.content,
      slug: datos.slug,
      country: datos.country,
    };

    actualizarPost({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarPost.success) {
        message.success("Marcado como me gusta");
        if (refetch != null) {
          refetch();
        }
      }
    });
  };

  return (
    <div className="post_container">
      {posts.length === 0 ? (
        <Nodata />
      ) : (
        <div className="post_container_grid">
          {posts &&
            posts.map((post: any, i: number) => {
              return (
                <div className="post_card" key={i}>
                  <div className="img_conts">
                    <img src={post.image} alt={post.title} />
                    <div className="like">
                      <button onClick={() => updatePost(post)}>
                        <LikeOutlined
                          style={{ color: "#90c33c", fontSize: 20 }}
                        />
                      </button>
                      <p>{post.like}</p>
                    </div>
                  </div>
                  <div className="info_posts">
                    <h1>{post.title}</h1>
                    <p>{post.shortDescription}</p>
                  </div>
                  <div style={{ paddingLeft: 10 }}>
                    <span>
                      <ClockCircleOutlined /> Lectura de {post.readTime}
                    </span>
                  </div>
                  <div style={{ margin: 10 }}>
                    <span>
                      <TagOutlined />
                      {post.tags &&
                        post.tags.map((t: string, i: number) => {
                          return (
                            <Tooltip title={`Ver posts sobre ${t}`} key={i}>
                              <Link to={`/tag/${t}`} className="tag">
                                {t},
                              </Link>
                            </Tooltip>
                          );
                        })}
                    </span>
                  </div>
                  <div style={{ paddingLeft: 10 }}>
                    <span>
                      <CalendarOutlined /> Publicado{" "}
                      {moment(post.created_at).format("LL")}
                    </span>
                  </div>

                  <div className="author">
                    <img
                      src={IMAGES_PATH + post.Author.avatar}
                      alt={post.Author.name}
                      className="avatar"
                    />
                    <div className="names">
                      <h4>
                        {post.Author.name} {post.Author.lastName}
                      </h4>
                      <p>Autor</p>
                    </div>
                  </div>

                  <div className="links">
                    <div>
                      <FacebookShareButton
                        url={"https://blog.wilbby.com/" + post.slug}
                      >
                        <FacebookIcon
                          size={30}
                          round={true}
                          style={{ marginLeft: 10, outline: "none" }}
                        />
                      </FacebookShareButton>

                      <TwitterShareButton
                        url={"https://blog.wilbby.com/" + post.slug}
                      >
                        <TwitterIcon
                          size={30}
                          round={true}
                          style={{ marginLeft: 10, outline: "none" }}
                        />
                      </TwitterShareButton>

                      <LinkedinShareButton
                        url={"https://blog.wilbby.com/" + post.slug}
                      >
                        <LinkedinIcon
                          size={30}
                          round={true}
                          style={{ marginLeft: 10, outline: "none" }}
                        />
                      </LinkedinShareButton>

                      <WhatsappShareButton
                        url={"https://blog.wilbby.com/" + post.slug}
                      >
                        <WhatsappIcon
                          size={30}
                          round={true}
                          style={{ marginLeft: 10, outline: "none" }}
                        />
                      </WhatsappShareButton>
                    </div>
                  </div>
                  <div className="bt_content">
                    <a href={post.content} target="_black">
                      Leer más
                    </a>
                  </div>
                </div>
              );
            })}
        </div>
      )}
    </div>
  );
}
