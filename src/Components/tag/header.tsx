import React from "react";
import Logo from "../../assets/img/logoblog.svg";
import { Link } from "react-router-dom";
import Curve from "../../assets/img/curve.svg";
import "./index.css";

export default function header(props: any) {
  const { tag, locality } = props;
  return (
    <div className="header_cont">
      <div className="header">
        <div className="logo">
          <Link to="/">
            <img src={Logo} alt="Wilbby Blog" />
          </Link>
        </div>
        <div className="Links">
          <a href="https://wilbby.com" className="buttons" target="_blak">
            Empezar
          </a>
        </div>
      </div>
      <div className="info_header">
        <div>
          <h2>Relacionado con {tag ? tag : ""}</h2>
          <p>
            Somos el equipo de prensa, contenido, investigación y diseño de
            Wilbby® App. Ayudamos a digitalizar las tiendas de barrio para que
            de está manera puedan llegar a más clientes con nuestra potente app.
          </p>
          <a href={`https://wilbby.com`}>
            Descubre establecimientos cerca de ti
          </a>
        </div>
      </div>
      <img src={Curve} style={{ marginBottom: -1 }} className="curve" />
    </div>
  );
}
