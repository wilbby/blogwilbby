import React, { useState, useEffect } from "react";
import Header from "./header";
import Post from "./Post";
import { withRouter } from "react-router";
import { DoubleRightOutlined, DoubleLeftOutlined } from "@ant-design/icons";
import Loading from "../Loading/Loading";
import "./index.css";

function Tag(props: any) {
  const [city, setCity] = useState("España");
  const [page, setPage] = useState(1);
  const [locality, setlocality] = useState("España");
  const tags = props.match.params.tag;

  useEffect(() => {
    document.title = `${tags}`;
  }, []);

  return (
    <div>
      <Header tag={tags} locality={locality} />
      <div className="post_containers">
        <div className="post_containers_post">
          <h1>Artículos relacionado con {tags}</h1>
          {city ? (
            <Post tag={tags} city={city} page={page} />
          ) : (
            <div className="post_container_grid">
              <Loading />
              <Loading />
              <Loading />
              <Loading />
              <Loading />
              <Loading />
            </div>
          )}
        </div>
        {page === 0 ? (
          <button className="btn_pagination" onClick={() => setPage(page + 1)}>
            Siguiente pagina <DoubleRightOutlined />
          </button>
        ) : (
          <>
            <button
              className="btn_pagination"
              onClick={() => {
                if (page === 0) {
                } else {
                  setPage(page - 1);
                }
              }}
            >
              <DoubleLeftOutlined /> Pagina anterior
            </button>
            <button
              className="btn_pagination"
              onClick={() => setPage(page + 1)}
            >
              Siguiente pagina <DoubleRightOutlined />
            </button>
          </>
        )}
      </div>
    </div>
  );
}

export default withRouter(Tag);
