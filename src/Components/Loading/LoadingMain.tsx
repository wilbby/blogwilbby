import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./index.css";

export default function LoadingMain() {
  return (
    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
      <div className="container__cards_main">
        <div>
          <Skeleton width="100%" height={350} />
        </div>
        <div>
          <p>
            <Skeleton count={1} width="90%" />
          </p>
          <p>
            <Skeleton count={1} width="80%" />
          </p>
          <p>
            <Skeleton count={1} width="75%" />
          </p>
          <p>
            <Skeleton count={1} width="90%" />
          </p>
          <p>
            <Skeleton count={1} width="80%" />
          </p>
          <p>
            <Skeleton count={1} width="75%" />
          </p>
          <p>
            <Skeleton count={1} width="90%" />
          </p>
          <p>
            <Skeleton count={1} width="80%" />
          </p>
          <p>
            <Skeleton count={1} width="75%" />
          </p>
        </div>
      </div>
    </SkeletonTheme>
  );
}
