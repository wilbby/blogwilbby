import React from "react";
import imagen1 from "../../assets/img/imagen1.jpg";
import imagen3 from "../../assets/img/imagen3.jpg";
import imagen2 from "../../assets/img/imagen2.jpg";
import "./index.css";

export default function Temas() {
  return (
    <div>
      <div className="Features">
        <div className="FeatureContainer">
          <img src={imagen1} alt="image1" />

          <div className="FeatureText">
            <h3>Líderes en municipios de menos de 50.000 habitantes</h3>
            <p>
              En el equipo de stores estamos ayudando activamente a revolucionar
              la industria del ecommerce y del delivery. Para ello, hemos creado
              las herramientas perfectas para optimizar las operaciones diarias
              de los establecimientos de tu ciudad: entregas ultra rápidas,
              desarrollo de Wilbby App, generación de comunidad.
            </p>
          </div>
        </div>

        <div className="FeatureContainer">
          <img src={imagen3} alt="image1" />

          <div className="FeatureText">
            <h3>Automatizamos tu marketing digital</h3>
            <p>
              Ponemos todos nuestros recursos y medios a tu servicio para
              ayudarte a reducir costes e impulsar tu negocio. Campañas de email
              marketing personalizas, mensajes push, microinfluencers, concursos
              y sorteos,…las mejores estrategias gratuitas digitales solo por
              ser parte de Wilbby.
            </p>
          </div>
        </div>

        <div className="FeatureContainer">
          <img src={imagen2} alt="image1" />

          <div className="FeatureText">
            <h3>Digitalizamos tu negocio</h3>
            <p>
              Sin duda, en los últimos años es de gran importancia tener
              presencia en el mundo online y con las soluciones que Wilbby®️ te
              presenta lo puedes hacer de la forma más económica. Ser parte de
              la plataforma es gratis, solo cobramos una comisión de las ventas
              que te ayudemos a conseguir. Mucho mejor que hacerte una página
              web de 3.000 euros ¿no?
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
