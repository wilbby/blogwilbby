import Geocode from "react-geocode";
import { GOOGLE_API_KEY } from "./UrlConfig";

export const getGeoLocation = (latitude: any, longitude: any) => {
  Geocode.setApiKey(GOOGLE_API_KEY);
  Geocode.setLanguage("es");
  Geocode.setRegion("es");
  //@ts-ignore
  Geocode.setLocationType("ROOFTOP");

  Geocode.fromLatLng(`${latitude}`, `${longitude}`).then(
    (response: any) => {
      const citys = localStorage.getItem("city");
      let city;
      for (let i = 0; i < response.results[0].address_components.length; i++) {
        for (
          let j = 0;
          j < response.results[0].address_components[i].types.length;
          j++
        ) {
          switch (response.results[0].address_components[i].types[j]) {
            case "locality":
              city = response.results[0].address_components[5].long_name;
              break;
          }
        }
      }
      if (!citys) {
        localStorage.setItem("city", city);
      }
    },
    (error: any) => {
      console.error(error);
    }
  );
};
