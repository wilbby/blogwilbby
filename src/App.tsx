import React, { useEffect } from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";

// Screen ///
import Home from "./Pages/Home";
import Footer from "./Components/Footer";
import NoFound from "./Pages/NoFound";
import Tag from "./Components/tag";
import GetApp from "./Components/GetApp";
import { getGeoLocation } from "./Utils/getCity";

function App() {
  useEffect(() => {
    if ("geolocation" in window.navigator) {
      window.navigator.geolocation.getCurrentPosition(
        (position) => {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          getGeoLocation(latitude, longitude);
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  });
  return (
    <BrowserRouter>
      <GetApp />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/tag/:tag" component={Tag} />
        <Route component={NoFound} />
      </Switch>

      <Footer />
    </BrowserRouter>
  );
}

export default App;
