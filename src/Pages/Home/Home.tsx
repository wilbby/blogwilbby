import React, { useState, useEffect } from "react";
import { DoubleRightOutlined, DoubleLeftOutlined } from "@ant-design/icons";
import "./index.css";
import Header from "../../Components/Header";
import Temas from "../../Components/temas";
import Franja from "../../Components/franja";
import MainPost from "../../Components/mainPost";
import Post from "../../Components/Post";
import CookieConsent from "react-cookie-consent";
import LoadingMain from "../../Components/Loading/LoadingMain";
import Loading from "../../Components/Loading/Loading";

export default function Home() {
  const [page, setPage] = useState(1);
  const [city, setCity] = useState("España");
  const [locatity, setlocatity] = useState("");

  useEffect(() => {}, [city]);
  return (
    <div>
      <Header city={city} locality={locatity} />
      <Temas />
      <Franja />
      <div className="post_containers">
        <div className="post_containers_post">
          <h1>Artículo Destacado</h1>
          {city ? <MainPost city={city} /> : <LoadingMain />}

          <h1>Últimos artículos</h1>

          {city ? (
            <Post city={city} page={page} />
          ) : (
            <div className="post_container_grid">
              <Loading />
              <Loading />
              <Loading />
              <Loading />
              <Loading />
              <Loading />
            </div>
          )}
        </div>
        {page === 1 ? (
          <button className="btn_pagination" onClick={() => setPage(page + 1)}>
            Siguiente pagina <DoubleRightOutlined />
          </button>
        ) : (
          <>
            <button
              className="btn_pagination"
              onClick={() => {
                if (page === 1) {
                } else {
                  setPage(page - 1);
                }
              }}
            >
              <DoubleLeftOutlined /> Pagina anterior
            </button>
            <button
              className="btn_pagination"
              onClick={() => setPage(page + 1)}
            >
              Siguiente pagina <DoubleRightOutlined />
            </button>
          </>
        )}
      </div>
      <CookieConsent
        location="bottom"
        buttonText="Aceptar"
        cookieName="myAwesomeCookieName2"
        style={{
          background: "#eeeeee",
          color: "black",
        }}
        buttonStyle={{
          color: "white",
          fontSize: "14px",
          backgroundColor: "#90C33C",
          borderRadius: 100,
          width: 150,
          outline: "none",
        }}
        expires={150}
      >
        Este sitio web utiliza cookies para garantizar que obtenga la mejor
        experiencia en nuestro sitio web.{" "}
        <span style={{ fontSize: "12px" }}>
          Más detalles en https://Wilbby.com/cookies
        </span>
      </CookieConsent>
    </div>
  );
}
