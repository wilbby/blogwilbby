import React from "react";
import "./index.css";
import Nodata from "../../Components/NoData";

export default function NoFound() {
  return (
    <div className="noFound">
      <Nodata />
      <a href="/">Volver a inicio</a>
    </div>
  );
}
