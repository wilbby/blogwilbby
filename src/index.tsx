import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./antd.css";
import { ApolloProvider } from "react-apollo";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import { LOCAL_API_URL, LOCAL_API_PATH } from "./Utils/UrlConfig";

// Configuración del Apollo Client
const client = new ApolloClient({
  uri: LOCAL_API_URL + LOCAL_API_PATH,
  // enviar token al servidor
  fetchOptions: {
    credentials: "include",
  },
  //@ts-ignore
  request: (operation) => {
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmQ2M2NkYWI3ZWY4NmE0YWZkZTE0YTAiLCJpYXQiOjE2MDgyMjEwODcsImV4cCI6MTYyNjIyMTA4N30.nBLGqdHkaQkOIqb8PY70shyk8LNN3pANmHFRSFxN7EA";
    operation.setContext({
      headers: {
        authorization: token,
      },
    });
  },
  cache: new InMemoryCache({
    addTypename: false,
  }),
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    // if(graphQLErrors && graphQLErrors[0].extensions.code === 'UNAUTHENTICATED'){
    //   message.error('Debe iniciar sesión para agregar un servicio.');
    // }
    console.log("networkError", networkError);
  },
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
